# Short introduction #

Generic part should be shared across all services that belongs to application. Depends on repository layout you can put it as separate as for rest. Another option is to put all of them into one repository. 

Service side is for particular service. 
To deploy code you need to:
* deploy generic with proper overlay - for example `generic/overlays/dev`
* deploy application with proper overlay - for example `service/overlays/dev`.